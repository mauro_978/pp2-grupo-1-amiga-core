package configuracion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import interfacesModelo.Notificador;
import interfacesModelo.Observable;
import interfacesModelo.Observer;

public class ConfiguradorDeNotificadores implements Observer{
	private Map<Notificador, Boolean>		notificadores;
	
	public ConfiguradorDeNotificadores(List<Notificador> notificadores) {
		this.notificadores = new HashMap<Notificador, Boolean>();
		for(Notificador n :notificadores) {
			this.notificadores.put(n, true);
		}
	}
	
	public Map<Notificador, Boolean> getNotificadores(){
		return this.notificadores;
	}
	
	public void habilitarDeshabilitarNotificador (Notificador not, boolean estado) {
		this.notificadores.put(not, estado);
	}

	@Override
	public void actualizar(Observable obs) {
		for(Notificador n : this.notificadores.keySet()) {
			if(notificadores.get(n)) {
				n.actualizar(obs);
			}
		}
	}
}
