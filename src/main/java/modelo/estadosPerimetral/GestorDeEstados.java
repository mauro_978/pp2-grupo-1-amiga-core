package modelo.estadosPerimetral;

public class GestorDeEstados {
	private Estado 		estado;
	private int 		distanciaPeligro;
	private int 		distanciaPrecaucion;
	private int			cantMaximaDeIntentos;
	
	public GestorDeEstados(int distanciaPeligro, int distanciaPrecaucion, int cantMaxInten) {
		this.estado = new EstadoDesconocido(this);
		this.cantMaximaDeIntentos = cantMaxInten;
		this.distanciaPeligro = distanciaPeligro;
		this.distanciaPrecaucion = distanciaPrecaucion;
	}
	
	public void cambiarEstado(Estado e) {
		this.estado = e;
	}
	
	public boolean validarEstado(int distanciaActual) {
		return this.estado.validarEstado(distanciaActual);
	}

	public int getDistanciaPeligro() {
		return distanciaPeligro;
	}

	public int getDistanciaPrecaucion() {
		return distanciaPrecaucion;
	}

	public int getCantMaximaDeIntentos() {
		return cantMaximaDeIntentos;
	}
	
	public String getEstadoActual() {
		return this.estado.getEstado();
	}
	
}
