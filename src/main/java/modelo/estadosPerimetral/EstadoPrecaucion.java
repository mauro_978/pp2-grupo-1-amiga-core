package modelo.estadosPerimetral;

public class EstadoPrecaucion extends Estado {
	private GestorDeEstados 		ctx;
	private String			estado;
	int						cantidadDeIntentos;
	
	public EstadoPrecaucion(GestorDeEstados ctx) {
		super(ctx);
		this.ctx = ctx;
		this.estado = "PRECAUCION";
		this.cantidadDeIntentos = 1;
	}
	
	@Override
	public boolean validarEstado(int DistanciaActual) {
		//Si la distancia es menor a peligro, cambio el estado a peligro
		if(DistanciaActual <= this.ctx.getDistanciaPeligro()) {
			this.ctx.cambiarEstado(new EstadoPeligro(this.ctx));
			return true; //notifico cambio de estado
		}
		//Si la distancia es menor a precaucion peron mayor a peigro, receteo el contador de cantidad de intentos
		if(DistanciaActual <= this.ctx.getDistanciaPrecaucion()) {
			this.cantidadDeIntentos = 1;
			return false; //notifico que no se cambio de estado
		}
		//Si la distancia es mayor a precaucion, veo la cantidad de intentos
		//Si la cantidad de intentos es mayor a la cant max cambio a estado Seguro
		if(this.cantidadDeIntentos >= this.ctx.getCantMaximaDeIntentos()) {
			this.ctx.cambiarEstado(new EstadoSeguro(this.ctx));
			return true; //notifico cambio de estado
		}
		//Si la cantidad de intentos es menor o igual aumento el contador de cantidad de intentos en 1 
		this.cantidadDeIntentos++;
		return false; //notifico que no se cambio de estado
	}
	@Override
	public String getEstado() {
		return estado;
	}
}
