package modelo.estadosPerimetral;

public class EstadoSeguro extends Estado{
	private GestorDeEstados 		ctx;
	private String			estado;
	
	public EstadoSeguro(GestorDeEstados ctx) {
		super(ctx);
		this.ctx = ctx;
		this.estado = "SEGURO";
		this.cantidadDeIntentos = 0;
	}
	
	@Override
	public boolean validarEstado(int DistanciaActual) {
		//Si la distancia es menor a la de peligro, pasa a peligro
		if(DistanciaActual <= this.ctx.getDistanciaPeligro()) {
			this.ctx.cambiarEstado(new EstadoPeligro(this.ctx));
			return true; //notifico el cambio de estado
		}
		//Si la distancia es menor a la de precaucion, pasa a precaucion
		if(DistanciaActual <= this.ctx.getDistanciaPrecaucion()) {
			this.ctx.cambiarEstado(new EstadoPrecaucion(this.ctx));
			return true; //notifico el cambio de estado
		}
		// Se queda en el estado actual
		return false; // notifico el no cambio de estado.
	}
	@Override
	public String getEstado() {
		return estado;
	}
}
