package modelo.estadosPerimetral;

public class EstadoPeligro extends Estado{
	private GestorDeEstados 		ctx;
	private String			estado;
	int						cantidadDeIntentos;
	
	public EstadoPeligro(GestorDeEstados ctx) {
		super(ctx);
		this.ctx = ctx;
		this.estado = "PELIGRO";
		this.cantidadDeIntentos = 1;
	}
	
	@Override
	public boolean validarEstado(int DistanciaActual) {
		//Si la distancia es menor a peligro, reseteo la cantidad de intentos
		if(DistanciaActual <= this.ctx.getDistanciaPeligro()) {
			this.cantidadDeIntentos = 1;
			return false; // notifico que no se cambio de estado
		}
		//Si la distancia es menor a precaucion, reviso la cantidad de intentos
		if(DistanciaActual <= this.ctx.getDistanciaPrecaucion()) {
			//Si la cantidad de intentos es menor a la cant max, aumento en uno el cont
			if(this.cantidadDeIntentos <= this.ctx.getCantMaximaDeIntentos()) {
				this.cantidadDeIntentos++;
				return false; //notifico que no se cambio de estado
			}
			//Si la cantidad de intentos es mayor a la canto max, cambio el estado a precaucion
			this.ctx.cambiarEstado(new EstadoPrecaucion(this.ctx));
			return true; //notifico el cambio de estado
		}
		//Si la distancia es mayor a precuacion cambio el estado a precaucion
		this.ctx.cambiarEstado(new EstadoPrecaucion(this.ctx));
		return true;
	}
	@Override
	public String getEstado() {
		return estado;
	}
}
