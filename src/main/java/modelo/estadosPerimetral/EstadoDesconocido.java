package modelo.estadosPerimetral;

public class EstadoDesconocido extends Estado {
	private GestorDeEstados 	ctx;
	private String				estado;
	int							cantidadDeIntentos;
	
	public EstadoDesconocido(GestorDeEstados ctx) {
		super(ctx);
		this.ctx = ctx;
		this.estado = "DESCONOCIDO";
		this.cantidadDeIntentos = 0;
	}
	
	@Override
	public boolean validarEstado(int DistanciaActual) {
		//Si la distancia es menor a peligro, cambio el estado a peligro
		if(DistanciaActual <= this.ctx.getDistanciaPeligro()) {
			this.ctx.cambiarEstado(new EstadoPeligro(this.ctx));
			return true; //notifico cambio de estado
		}
		//Si la distancia es menor a precaucion cambio el estado a precacuion
		if(DistanciaActual <= this.ctx.getDistanciaPrecaucion()) {
			this.ctx.cambiarEstado(new EstadoPrecaucion(this.ctx));
			return true; //notifico que no se cambio de estado
		}
		//Si la distnacia es meyor a precuación, el estado pasa a seguro
		this.ctx.cambiarEstado(new EstadoSeguro(this.ctx));
		return true; //notifico que no se cambio de estado
	}
	@Override
	public String getEstado() {
		return estado;
	}

}
