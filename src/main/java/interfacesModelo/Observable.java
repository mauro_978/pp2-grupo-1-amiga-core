package interfacesModelo;

public interface Observable {
	
	void attach(Observer o);
	void dettach(Observer o);
	void notificar(Observable o);
}
