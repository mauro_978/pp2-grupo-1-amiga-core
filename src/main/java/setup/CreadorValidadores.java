package setup;

import java.util.Map;

import interfacesModelo.ServicioCalculadorDeDistancias;
import modelo.Dispositivo;
import modelo.Perimetral;
import modelo.Persona;
import modelo.Validador;

public class CreadorValidadores {
	Map<Persona, Dispositivo>				dispositivos;
	ServicioCalculadorDeDistancias scd;
	
	public CreadorValidadores(Map<Persona, Dispositivo> dispositivos, ServicioCalculadorDeDistancias scd) {
		this.dispositivos = dispositivos;
		this.scd = scd;
	}
	
	public Validador crearValidador(Perimetral p) {
		return new Validador(dispositivos.get(p.getAgredido()), dispositivos.get(p.getAgresor()), p, scd);
	}
}
