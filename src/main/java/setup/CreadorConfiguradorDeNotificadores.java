package setup;

import java.io.File;
import java.util.List;

import configuracion.ConfiguradorDeNotificadores;
import configuracion.DescubridorDeNotificadores;
import interfacesModelo.Notificador;

public class CreadorConfiguradorDeNotificadores {
	public CreadorConfiguradorDeNotificadores() {}
	public ConfiguradorDeNotificadores getConfiguradorDeNotificadores() {
		String pathDefault = new File("").getAbsolutePath().toString()+"\\bin\\main\\plugins";	
		List<Notificador> nots = DescubridorDeNotificadores.getNotificadores(pathDefault, "plugins");
		ConfiguradorDeNotificadores CDN = new ConfiguradorDeNotificadores(nots);
		return CDN;
	}
}
