package setup;

import java.util.HashMap;
import java.util.Set;


import modelo.Dispositivo;
import modelo.Perimetral;
import modelo.Persona;

public class CreadorDeMapDispositivos {
	
	public CreadorDeMapDispositivos() {}
	
	public HashMap<Persona, Dispositivo> crearDispositivos(Set<Perimetral> perimetrales){
		CreadorDispositivos cd = new CreadorDispositivos();
		HashMap<Persona, Dispositivo> dispositivos = new HashMap<Persona, Dispositivo>();
		for(Perimetral p : perimetrales) {
			dispositivos.put(p.getAgredido(), cd.crearDispositivo(p.getAgredido()));
			dispositivos.put(p.getAgresor(), cd.crearDispositivo(p.getAgresor()));
		}
		return dispositivos;
	}
}
