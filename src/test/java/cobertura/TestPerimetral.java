package cobertura;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import modelo.Perimetral;
import modelo.Persona;

public class TestPerimetral {
	
	Persona agresor, agredido;
	int distanciaPeligro;
	int distanciaPrecaucion;
	Perimetral p;
	
	@Before
	public void setUp() {
		agresor = new Persona("40698875","juan","D1");
		agredido = new Persona("42853431","pedro","D2");
		distanciaPeligro = 10;
		distanciaPrecaucion = 20;
		p = new Perimetral(agresor, agredido, distanciaPeligro, distanciaPrecaucion);
	}
	
	@Test
	public void testGetId() {
		assertTrue(p.getId()==0);
	}

	@Test
	public void testGetAgresor() {
		assertEquals(p.getAgresor(), agresor);
		assertNotEquals(p.getAgresor(), agredido);
	}

	@Test
	public void testGetAgredido() {
		assertEquals(p.getAgredido(), agredido);
		assertNotEquals(p.getAgredido(), agresor);
	}
	
	@Test
	public void testGetDistanciaPeligro() {
		assertTrue(p.getDistanciaPeligro()==distanciaPeligro);
	}
	
	@Test
	public void testGetDistanciaPrecaucion() {
		assertTrue(p.getDistanciaPrecaucion()==distanciaPrecaucion);
	}

}
