package cobertura;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import modelo.Dispositivo;
import modelo.Persona;

public class TestsCobertura {
	
	@Before
	public void setup() {}
	

	@Test
	public void equalsPersona() {
		Persona persona1 = new Persona("1", "Jose", "Gomez");
		Persona persona2 = new Persona("1", "Jose", "Gomez");
		Persona persona3 = new Persona("2", "Jose", "Martinez");
		Persona persona4 = new Persona(null,"Jose", "Gimenez");
		assertTrue(persona1.equals(persona1));
		assertTrue(persona1.equals(persona2));
		assertFalse(persona1.equals(persona3));
		assertFalse(persona1.equals(persona4));
		assertFalse(persona1.equals(null));
	}
	
	@Test
	public void equalsDispositivo() {
		Dispositivo d1 = new Dispositivo("1");
		Dispositivo d2 = new Dispositivo("1");
		Dispositivo d3 = new Dispositivo("2");
		Dispositivo d4 = new Dispositivo(null);
		assertTrue(d1.equals(d1));
		assertTrue(d1.equals(d2));
		assertFalse(d1.equals(d4));
		assertFalse(d1.equals(d3));
		assertFalse(d1.equals(null));
	}

}
