package cobertura;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import modelo.Persona;

public class TestPersona {

	Persona p;
	
	@Before
	public void SetUp() {
		p = new Persona("40698875","juan","D1");
	}
	
	@Test
	public void testGetNombre() {
		assertEquals(p.getNombre(), "juan");
	}
	
	@Test
	public void testSetNombre() {
		p.setNombre("pedro");
		assertEquals(p.getNombre(), "pedro");
	}
	
	@Test
	public void testGetDni() {
		assertEquals(p.getDni(), "40698875");
	}
	
	@Test
	public void testGetIdDispositivo() {
		assertEquals(p.getIdDispositivo(), "D1");
	}
	
	@Test
	public void testEquals() {
		//pruebo con personas iguales
		Persona p2 = new Persona("40698875","juan","D1");
		assertTrue(p.equals(p2));
		
		//pruebo con personas con mismo dni y idDispositivo pero distindo nombre
		Persona p3 = new Persona("40698875","pedro","D1");
		assertTrue(p.equals(p3));
		
		//pruebo con personas con mismo dni pero idDispositivo y nombre distintos
		Persona p4 = new Persona("40698875","pedro","D2");
		assertTrue(p.equals(p4));
		
		//pruebo con personas con mismo nombre pero dni y idDispositivo distintos
		Persona p5 = new Persona("45132262","juan","D2");
		assertFalse(p.equals(p5));
		
		//pruebo con personas con mismo idDispositivo pero dni y nombre distintos
		Persona p6 = new Persona("45132262","pedro","D1");
		assertFalse(p.equals(p6));
		
		//pruebo con una persona con dni null
		Persona p7 = new Persona(null,"pedro","D1");
		assertFalse(p.equals(p7));
		
		//pruebo con con la primera persona con dni null y la otra persona con dni distinto de null
		Persona p8 = new Persona(null,"pedro","D1");
		assertFalse(p8.equals(p));
		
		//pruebo con 2 personas con dni null
		Persona p9 = new Persona(null,"pedro","D1");
		Persona p10 = new Persona(null,"juan","D3");
		assertTrue(p9.equals(p10));
		
		//pruebo con un objeto que no es una persona
		String p11 = new String("40698875");
		assertFalse(p.equals(p11));
	}

}
