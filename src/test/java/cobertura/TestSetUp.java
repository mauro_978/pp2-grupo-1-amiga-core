package cobertura;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import configuracion.ConfiguradorDeNotificadores;
import modelo.MonitoreoDispositivos;
import modelo.MonitoreoValidadores;
import setup.Setup;

public class TestSetUp {

	Setup setUp;
	
	@Before
	public void SetUp() {
		setUp = new Setup("Escenario.json");
	}
	
	@Test
	public void testGetMonitoreoDispositivo() {
		assertTrue(setUp.getMonitoreoValidadores() instanceof MonitoreoDispositivos);
	}
	
	@Test
	public void testGetMonitoreoValidadores() {
		assertTrue(setUp.getMonitoreoDispositivo() instanceof MonitoreoValidadores);
	}
	
	@Test
	public void testGetConfiguradorDeNotificadores() {
		assertTrue(setUp.getConfiguradorDeNotificadores() instanceof ConfiguradorDeNotificadores);
	}

}
