package criteriosDeAceptacion;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import configuracion.ConfiguradorDeNotificadores;
import interfacesModelo.Notificador;
import interfacesModelo.Observable;

public class CriteriosDeAceptacionUS3 {
	
	ConfiguradorDeNotificadores cn;
	Notificador mecanismoDeNotificacionEmail, mecanismoDeNotificacionTelegram,
				nEMailSpy, nTelegramSpy;
	
	/*Se inicializan los objetos necesarios para probar la US3*/
	@Before
	public void SetUp() {
		//creo un mecanismo de notificacion por Email
		mecanismoDeNotificacionEmail = new Notificador() {
			
			String nombre = "Email";
			@Override
			public void actualizar(Observable obs) {
//				System.out.println("Notifico por Email");
			}
			@Override
			public String getNombre() {
				return nombre;
			}};
		
		//creo un mecanismo de notificacion por Telegram
		mecanismoDeNotificacionTelegram = new Notificador() {
				
			String nombre = "Telegram";
			@Override
			public void actualizar(Observable obs) {
//				System.out.println("Notifico por Telegram");
			}
			@Override
			public String getNombre() {
				return nombre;
			}};
		
		/*para verificar cuantas veces se llamo al metodo actualizar de un mecanismo 
		 *de notificacion (cuantas veces se indico al notificador que notifique) se 
		 *utilizara un spy el cual nos proporciona Mockito*/	
			
		//creo un spy a partir del mecanismo de notificacion por Email
		nEMailSpy = Mockito.spy(mecanismoDeNotificacionEmail);
		
		//creo un spy a partir del mecanismo de notificacion por Telegram
		nTelegramSpy = Mockito.spy(mecanismoDeNotificacionTelegram);
		
		/*creo una lista de notificadores, agregando los spy en lugar de los
		 *notificadores "reales"*/
		ArrayList<Notificador> notificadores = new ArrayList<Notificador>();
		notificadores.add(nEMailSpy);
		notificadores.add(nTelegramSpy);
		
		//creo el configurador de notificadores con la lista de notificadores con los spy
		cn = new ConfiguradorDeNotificadores(notificadores);
		
	}
	
	/****************************************************************/
	/*En todos los Escenarios se usaran 2 mecanismos de notificacion*/
	/*                    [Email - Telegram]                        */
	/****************************************************************/
	
	/*En el Escenario1, tanto el mecanismo de notificacion 
	 *de mail como el de telegram se encuentran habilitados*/
	public void Escenario1() {
		
		cn.habilitarDeshabilitarNotificador(nEMailSpy, true);
		cn.habilitarDeshabilitarNotificador(nTelegramSpy, true);
		
    } 
	
	/*En el Escenario2, el mecanismo de notificacion por email se encuentra deshabilitado,
	 *mientras que el de telegram se encuentra habilitado*/
	public void Escenario2() {
		
		cn.habilitarDeshabilitarNotificador(nEMailSpy, false);
		cn.habilitarDeshabilitarNotificador(nTelegramSpy, true);
    } 
	
	/*En el Escenario3, el mecanismo de notificacion por telegram se encuentra deshabilitado,
	 *mientras que el de mail se encuentra habilitado*/
	public void Escenario3() {
		
		cn.habilitarDeshabilitarNotificador(nEMailSpy, true);
		cn.habilitarDeshabilitarNotificador(nTelegramSpy, false);
    } 

	
	@Test
	public void ca1() {
		Escenario1();
		
		//confirmo que se encuentra habilitado el mecanismo de notificacion por mail
		assertTrue(cn.getNotificadores().get(nEMailSpy));
		
		//confirmo que se encuentra habilitado el mecanismo de notificacion por telegram
		assertTrue(cn.getNotificadores().get(nTelegramSpy));
		
//		//le digo al configurador de notificadores que ocurrio un cambio y que debe notificar por los mecanismos
//		//de notificacion que se encuentren habilitados
//		cn.actualizar(null);
//		
//		//verifico que notifico 1 vez al mecanismo de notificacion por email 
//		verify(nEMailSpy, times(1)).actualizar(Mockito.any());
//		
//		//verifico que notifico 1 vez al mecanismo de notificacion por telegram
//		verify(nTelegramSpy, times(1)).actualizar(Mockito.any());
		
	}
	
	@Test
	public void ca2() {
		Escenario1();	
		
		//confirmo que se encuentra habilitado el mecanismo de notificacion por mail
		assertTrue(cn.getNotificadores().get(nEMailSpy));
		
		//lo intento deshabilitar
		cn.habilitarDeshabilitarNotificador(nEMailSpy, false);
		
		//confirmo que se encuentra deshabilitado el mecanismo de notificacion por mail
		assertFalse(cn.getNotificadores().get(nEMailSpy));
		
		//confirmo que se encuentra habilitado el mecanismo de notificacion por telegram
		assertTrue(cn.getNotificadores().get(nTelegramSpy));
				
//		//le digo al configurador de notificadores que ocurrio un cambio y que debe notificar por los mecanismos
//		//de notificacion que se encuentren habilitados
//		cn.actualizar(null);
//				
//		//verifico que notifico 0 veces (nunca notifico) al mecanismo de notificacion por email 
//		verify(nEMailSpy, times(0)).actualizar(Mockito.any());
//				
//		//verifico que notifico 1 vez al mecanismo de notificacion por telegram
//		verify(nTelegramSpy, times(1)).actualizar(Mockito.any());
		
	}
	
	@Test
	public void ca3() {
		Escenario2();
				
		//confirmo que se encuentra deshabilitado el mecanismo de notificacion por mail
		assertFalse(cn.getNotificadores().get(nEMailSpy));
		
		//lo intento deshabilitar
		cn.habilitarDeshabilitarNotificador(nEMailSpy, false);
		
		
		//confirmo que sigue deshabilitado
		assertFalse(cn.getNotificadores().get(nEMailSpy));
		
		//confirmo que se encuentra habilitado el mecanismo de notificacion por telegram
		assertTrue(cn.getNotificadores().get(nTelegramSpy));
				
				
//		//le digo al configurador de notificadores que ocurrio un cambio y que debe notificar por los mecanismos
//		//de notificacion que se encuentren habilitados
//		cn.actualizar(null);
//				
//		//verifico que notifico 0 veces (nunca notifico) al mecanismo de notificacion por email 
//		verify(nEMailSpy, times(0)).actualizar(Mockito.any());
//				
//		//verifico que notifico 1 vez al mecanismo de notificacion por telegram
//		verify(nTelegramSpy, times(1)).actualizar(Mockito.any());
		
	}
	
	@Test
	public void ca4() {
		Escenario3();	
				
		//confirmo que se encuentra deshabilitado el mecanismo de notificacion por telegram
		assertFalse(cn.getNotificadores().get(nTelegramSpy));
				
		//lo intento habilitar
		cn.habilitarDeshabilitarNotificador(nTelegramSpy, true);
				
				
		//confirmo que el mecanismo de notificacion por telegram pasa a estar habilitado
		assertTrue(cn.getNotificadores().get(nEMailSpy));
						
		//confirmo que se encuentra habilitado el mecanismo de notificacion por telegram
		assertTrue(cn.getNotificadores().get(nTelegramSpy));
						
//		//le digo al configurador de notificadores que ocurrio un cambio y que debe notificar por los mecanismos
//		//de notificacion que se encuentren habilitados
//		cn.actualizar(null);
//						
//		//verifico que notifico 1 vez al mecanismo de notificacion por email 
//		verify(nEMailSpy, times(1)).actualizar(Mockito.any());
//						
//		//verifico que notifico 1 vez al mecanismo de notificacion por telegram
//		verify(nTelegramSpy, times(1)).actualizar(Mockito.any());
		
	}
	
	@Test
	public void ca5() {
		Escenario1();
				
		//confirmo que se encuentra habilitado el mecanismo de notificacion por telegram
		assertTrue(cn.getNotificadores().get(nTelegramSpy));
						
		//lo intento habilitar
		cn.habilitarDeshabilitarNotificador(nTelegramSpy, true);
						
						
		//confirmo que sigue habilitado
		assertTrue(cn.getNotificadores().get(nTelegramSpy));
								
								
//		//le digo al configurador de notificadores que ocurrio un cambio y que debe notificar por los mecanismos
//		//de notificacion que se encuentren habilitados
//		cn.actualizar(null);
//								
//		//verifico que notifico 1 vez al mecanismo de notificacion por email 
//		verify(nEMailSpy, times(1)).actualizar(Mockito.any());
//								
//		//verifico que notifico 1 vez al mecanismo de notificacion por telegram
//		verify(nTelegramSpy, times(1)).actualizar(Mockito.any());
		
	}
	
	@Test
	public void ca6() {
		Escenario1();
			
		//le digo al configurador de notificadores que ocurrio un cambio (genero un evento) y que debe notificar por los mecanismos
		//de notificacion que se encuentren habilitados
		cn.actualizar(null);
								
		//verifico que notifico 1 vez al mecanismo de notificacion por email 
		verify(nEMailSpy, times(1)).actualizar(Mockito.any());
								
		//verifico que notifico 1 vez al mecanismo de notificacion por telegram
		verify(nTelegramSpy, times(1)).actualizar(Mockito.any());
		
	}

	@Test
	public void ca7() {
		Escenario2();
			
		//le digo al configurador de notificadores que ocurrio un cambio (genero un evento) y que debe notificar por los mecanismos
		//de notificacion que se encuentren habilitados
		cn.actualizar(null);
								
		//verifico que notifico 0 veces (nunca notifico) al mecanismo de notificacion por email 
		verify(nEMailSpy, times(0)).actualizar(Mockito.any());
								
		//verifico que notifico 1 vez al mecanismo de notificacion por telegram
		verify(nTelegramSpy, times(1)).actualizar(Mockito.any());
		
	}
	
}
