package criteriosDeAceptacion;

import static org.junit.Assert.*;
import org.junit.Test;

import interfacesModelo.Observable;
import interfacesModelo.Observer;
import interfacesModelo.ServicioCalculadorDeDistancias;
import modelo.Dispositivo;
import modelo.Perimetral;
import modelo.Persona;
import modelo.Ubicacion;
import modelo.Validador;
import servicio.CalculadorDeDistancias;

public class CriteriosDeAceptacionUS5 {

	Persona per1,per2;
	Perimetral p;
	Dispositivo d1,d2;
	ServicioCalculadorDeDistancias scd;
	Validador v;
	
	public void SetUpEscenario() {
		per1 = new Persona("45019951","juan","D1");
		per2 = new Persona("40219951","pedro","D2");
		p = new Perimetral(per1, per2, 5, 10);
		d1 = new Dispositivo(per1.getIdDispositivo());
		d2 = new Dispositivo(per2.getIdDispositivo());
		scd = new CalculadorDeDistancias() {
			@Override 
			public double distanciaEntre(Ubicacion u1, Ubicacion u2){
				double distancia = u1.getLongitud()-u2.getLongitud();
				if(distancia>=0)return distancia;
				return distancia * -1.0;
			}};
		v = new Validador(d1,d2,p,scd);
		Observer o = new Observer() {

			@Override
			public void actualizar(Observable obs) {
				// TODO Auto-generated method stub
				
			}};
		v.attach(o);
	}
	
	public void EscenarioDesconocido() {  
		SetUpEscenario();
	} 
	
	public void EscenarioSeguro() {  
		SetUpEscenario();
		Ubicacion nuevaUbicacion = new Ubicacion(0.0, 0.0);
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 15.0);
		d1.actualizarUbicacion(nuevaUbicacion);
		d2.actualizarUbicacion(nuevaUbicacion2);
    }
	
	public void EscenarioPrecaucion() {
		SetUpEscenario();
		Ubicacion nuevaUbicacion = new Ubicacion(0.0, 0.0);
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 9.0);
		d1.actualizarUbicacion(nuevaUbicacion);
		d2.actualizarUbicacion(nuevaUbicacion2);
    }
	
	public void EscenarioPeligro() {  
		SetUpEscenario();
		Ubicacion nuevaUbicacion = new Ubicacion(0.0, 0.0);
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 3.0);
		d1.actualizarUbicacion(nuevaUbicacion);
		d2.actualizarUbicacion(nuevaUbicacion2);
    }
	
	@Test 
	public void ca1() {
		EscenarioDesconocido();
		Ubicacion nuevaUbicacion = new Ubicacion(0.0, 0.0);
		d1.actualizarUbicacion(nuevaUbicacion);
		assertEquals("DESCONOCIDO", v.getEstadoPerimetral());
    }
	
	
	@Test
	public void ca2() {
		EscenarioDesconocido();
		Ubicacion nuevaUbicacion = new Ubicacion(0.0, 0.0);
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 14.0);
		
		d1.actualizarUbicacion(nuevaUbicacion);
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("SEGURO", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca3() {
		EscenarioDesconocido();
		Ubicacion nuevaUbicacion = new Ubicacion(0.0, 0.0);
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 8.0);
	
		d1.actualizarUbicacion(nuevaUbicacion);
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("PRECAUCION", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca4() {
		EscenarioDesconocido();
		Ubicacion nuevaUbicacion = new Ubicacion(0.0, 0.0);
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 2.0);
		
		d1.actualizarUbicacion(nuevaUbicacion);
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("PELIGRO", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca5() {
		EscenarioSeguro();
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 20.0);
		
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("SEGURO", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca6() {
		EscenarioSeguro();
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 7.0);
		
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("PRECAUCION", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca7() {
		EscenarioSeguro();
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 3.0);
		
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("PELIGRO", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca8() {
		EscenarioPrecaucion();
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 6.0);
		
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("PRECAUCION", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca9() {
		EscenarioPrecaucion();
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 20.0);
		
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("PRECAUCION", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca10() {
		EscenarioPrecaucion();
		Ubicacion nuevaUbicacion1 = new Ubicacion(0.0, 20.0);
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 21.0);
		Ubicacion nuevaUbicacion3 = new Ubicacion(0.0, 23.0);
		
		d2.actualizarUbicacion(nuevaUbicacion1);
		d2.actualizarUbicacion(nuevaUbicacion2);
		d2.actualizarUbicacion(nuevaUbicacion3);
		
		assertEquals("SEGURO", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca11() {
		EscenarioPrecaucion();
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 3.0);
		
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("PELIGRO", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca12() {
		EscenarioPeligro();
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 3.0);
		
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("PELIGRO", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca13() {
		EscenarioPeligro();
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 7.0);
		
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("PELIGRO", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca14() {
		EscenarioPeligro();
		Ubicacion nuevaUbicacion1 = new Ubicacion(0.0, 6.0);
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 7.0);
		Ubicacion nuevaUbicacion3 = new Ubicacion(0.0, 9.0);
		
		d2.actualizarUbicacion(nuevaUbicacion1);
		d2.actualizarUbicacion(nuevaUbicacion2);
		d2.actualizarUbicacion(nuevaUbicacion3);
		
		assertEquals("PRECAUCION", v.getEstadoPerimetral());
	}
	
	@Test
	public void ca15() {
		EscenarioPeligro();
		Ubicacion nuevaUbicacion2 = new Ubicacion(0.0, 25.0);
		
		d2.actualizarUbicacion(nuevaUbicacion2);
		
		assertEquals("PRECAUCION", v.getEstadoPerimetral());
	}

}
